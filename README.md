## The Patriarchy Harms Everybody

Toxic masculinity, gender roles and stereotyping affect everyone. *Patriarchy does
not discriminate.*

- In 2018, *3.85x more men* died by suicide compared to women.
  
  This is largely due to stigma surrounding mental health and the patriarchal notion
  that men should contain their emotions to be percieved as "strong" and "masculine".
  
- *1 in 5 women* and *1 in 38 men* are victims of attempted or successful rape, but 15
  out of 16 rapists walk free.
  
  This is caused by a mixture of factors, including:
  
  a) survivor's *shame* and *fear* to report due to public stigma surrounding sexual
    violence, for both men and women.
    
  b) a growing *victim blaming culture* (e.g "what were they wearing").
  
  c) law enforcement and justice systems that *don't convict* or prevent repeated abuses.
  
- *1 in 3 women* and *1 in 4 men* have faced physical violence at the hands of their
  partners at some point in their lives.
  
  Men are less likely to recognise or report their *abuse*, whilst women are more likely
  to experience severe and repeated abuse.

Please [make a pull request](https://github.com/queersocial/feminism.lgbt/pulls) to improve this page.
